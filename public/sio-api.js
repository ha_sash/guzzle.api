var GuzzleAPI = function(config, socketio) {

    /**
     * @property defaultNamespace {string} - Неймспейс поумолчанию
     * @property defaultApiCallEventName {string} - Имя события для вызова функций на сервере (используется если нет в конфиге)
     * @property socket {Object} - SocketIO коннект с сервером (для всего API подразумевается одно соединение)
     * @property listeners {Object} - список обработчиков
     * @property me {GuzzleAPI} - SocketIO коннект с сервером (для всего API подразумевается одно соединение)
     */
    var defaultNamespace = 'GAPI';
    var defaultApiCallEventName = 'api:call';
    var defaultApiResponseEventName = 'api:response';
    var socket = socketio;
    var listeners = {};
    var me = this;

    /**
     * Список имен событий возращающих ответ после вызова функции,
     * у каждого провайдера моэет быть собственное событие.
     * @type {object}
     */
    var handleResponceEventName = {};

    /**
     * Список обработчиков (ключ - ключ вызова : функция обработки результата)
     * @type {object}
     */
    var handlers = {};

    if (!Function.prototype.bind) {
        throw new Error('Browser does not support function bind.');
    }

    /**
     * Часть генератора GUID
     * @return {string}
     */
    var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };

    if (!(socket instanceof io.Socket) && config instanceof Object && config.url) {
        socket = io.connect(config.url);
    }

    if (!(socket instanceof io.Socket)) {
        throw new Error('Not the correct type of the adapter');
    }

    socket.on('message', function(msg) {
        var handler, info, callbacks;
        //console.log(msg);
        if (msg.event !== undefined && msg.id !== undefined && handleResponceEventName[msg.event] && (handler = handlers[msg.id].handler)) {
            handler(msg.result, msg);
            info = handlers[msg.id].info;

            callbacks = listeners[info.apiManager.getResultEventNameByMethodContext(info, 'response')];

            if (callbacks && callbacks.length > 0) {
                for (var i=0;i<callbacks.length;i++) {
                    callbacks[0](msg.result, msg, info);
                }
            }

            delete handlers[msg.id];
            delete handler;
            delete info;
        }
    });

    socket.on('connect', function(msg) {
        console.log('SIO connected...');
    });

    socket.on('disconnect', function(msg) {
        //socket = io.connect(config.url);
    });

    /**
     * Добавление обертки метода к API
     *
     * @param methodsConfig {Object}
     * @param ns {Object}
     * @param actionName {string}
     * @param configProvider {Object}
     */
    var addMethods = function(methodsConfig, ns, actionName, configProvider) {
        for(var i=0;i<methodsConfig.length;i++) {
            var methodConfig = methodsConfig[i];
            methodConfig.action = actionName;

            if (methodConfig instanceof Object && methodConfig.method !== undefined) {
                if (methodConfig.arguments === undefined) {
                    methodConfig.arguments = [];
                }
                ns[methodConfig.method] = createMethod(methodConfig, configProvider);
            }
        }
    };

    /**
     * Вернет префикс имени для метода апи или целиком имя
     * @param context
     * @return {string}
     */
    this.getResultEventNameByMethodContext = function(context, eventName) {
        var cfg = context.config, en = cfg.action + ':' + cfg.method;
        if (eventName !== undefined) en += ':' + eventName;
        return en;
    }

    /**
     * Добавление обработки конкретного вызова
     * @param id {string} - идентификатор вызова
     * @param fn {function} - обработчик ответа
     * @param methodInfo {object} - объект с информацией о методе
     */
    this.addHandler = function(id, fn, methodInfo) {
        handlers[id] = {
            handler: (fn instanceof Function) ? fn : function(){},
            info: methodInfo
        };
    }

    /**
     * Добавляем обработчики например для перманентной обработки возврата результатов нужных там методов апи.
     * @param eventName {string} - имя события
     * @param handler {Function} - функция обработчик
     * @return {boolean}
     */
    this.addListener = function(eventName, handler) {
        if (handler instanceof Function) {
            if (listeners[eventName] === undefined) {
                listeners[eventName] = [];
            }

            listeners[eventName].push(handler)
            return true;
        }

        return false;
    };

    /**
     * Фабрика оберток
     *
     * @param methodConfig {Object}
     * @param configProvider {Object}
     * @return {function} - функция скрывает за собой таинство обращения к серверу
     */
    var createMethod = function(methodConfig, configProvider) {
        var context = {apiManager: me, config: methodConfig, provider: configProvider};
        var method = function() {
            var argsLength = this.config.arguments.length,
                realArgs   = Array.prototype.slice.call(arguments, 0, argsLength),
                callback   = arguments[argsLength];

            if (this.config.arguments.length != realArgs.length) {
                throw new Error('Necessary amount of arguments ' + argsLength);
            } else {
                var socket = me.getSocket();

                if (!(socket instanceof io.Socket)) {
                    throw new Error('Not the correct type of the adapter');
                }

                var id = me.guid();
                var remoteEvent = {
                    event:  this.provider.calleventname,
                    id:     id,
                    action: this.config.action,
                    method: this.config.method,
                    args:   realArgs
                }

                this.apiManager.addHandler(id, callback, this);
                socket.send(remoteEvent);
            }
        }.bind(context);

        handleResponceEventName[configProvider.responseeventname] = true;

        method.on = function(eventName, handler) {
            if (handler instanceof Function) {
                handler.bind(this);
                this.apiManager.addListener(me.getResultEventNameByMethodContext(this, eventName), handler);
                return true;
            }

            return false;
        }.bind(context);

        return method;
    };

    /**
     * Создаст объект в глобальной области видимости.
     * можно описать разные уровни вложенности используя точки
     * например:
     * <code>
     * ns('Ns.OK.trololo')
     *     //добавит такую структуру
     *     Ns = {
     *         OK: {
     *             trololo: {
     *             }
     *         }
     *     }
     * </code>
     *
     * @param namespace {string}
     * @return {*}
     */
    this.ns = function() {
        var a=arguments, o=null, i, j, d;
        for (i=0; i<a.length; i=i+1) {
            d=a[i].split(".");
            o=window;
            for (j=0; j<d.length; j=j+1) {
                o[d[j]]=o[d[j]] || {};
                o=o[d[j]];
            }
        }
        return o;
    };

    /**
     * Получить имя события вызова серверной функции используемое поумолчанию.
     * @return {string}
     */
    this.getDefaultApiCallEventName = function() {
        return defaultApiCallEventName;
    };

    /**
     * Получить имя события возврата значения серверной функции используемое поумолчанию.
     * @return {string}
     */
    this.getDefaultApiResponseEventName = function() {
        return defaultApiResponseEventName;
    };

    /**
     * Добавить набор экшенов API
     * @param conf {Object}
     * @return {boolean}
     */
    this.addProvider = function(conf) {
        if (conf instanceof Object && config.actions && config.actions instanceof Object) {
            var ns = this.ns(config.namespace || defaultNamespace);

            if (conf.calleventname === undefined) {
                conf.calleventname = this.getDefaultApiCallEventName();
            }

            if (conf.responseeventname === undefined) {
                conf.responseeventname = this.getDefaultApiResponseEventName();
            }

            for(var i in conf.actions) {
                var action = conf.actions[i];
                ns[i] = {};
                addMethods(action, ns[i], i, conf);
            }

            ns.$APIManager = this;
            return true;
        }
        return false;
    };

    /**
     * Адептер соединения с сервером
     * @return {io.Socket}
     */
    this.getSocket = function() {
        return socket;
    };

    /**
     * Генерим GUID
     * @return {string}
     */
    this.guid = function() {
        return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
    };

    if (config !== undefined) {
        this.addProvider(config);
    }

};