# Socket.IO API #

## Создание менеджера API ##

Объект будет досутепн по адресу Guzzle.APIManager
В файле: ./bin/www для инициализации апи прописано следующее</p><pre>//загрузить файлы нужно до загрузки app

```
#!javascript

require('guzzle.api');

//Ну а это дело уже после того как создан сервер
Guzzle.IO = require('socket.io')(server);
Guzzle.APIManager.init(Guzzle.IO);

```

Подключение апи в браузере
Нужно подключить: sio-api.js, /socket.io/socket.io.js
И собственно само api.js, это аналог WSDL.

Примерно так:

```
#!html

<script src="/socket.io/socket.io.js"></script>
<script src="/sio-api.js"></script>
<script src="/demo/api.js">

```


В демо контроллере есть соответсвующий роутер который демонстрирует как
создать скрипт с описанием апи. (браузеру оно доступно по адресу /demo/api.js)


```
#!html

router.get('/api.js', function(req, res, next) {
   res.setHeader('content-type', 'application/javascript');
   res.end(Guzzle.APIManager.getScript());
});
```


-----

## Пример публикации объекта ##
### Самый простой объект ###
Данный объект просто накручивает счетчик...
Вариант с комментариями можно посмотреть в коде demo/index.js
Объект на сервере:

```
#!javascript

Ext.define('Counter', {
    counter: 0,

    get: function(result) {
        if (result.isResult()) {
            result.setData(this.counter++).send();
        }
    }
});

```

Вызов на клиенте:
```
#!javascript
Guzzle.Counter.get(function(result, eventMessage) {console.log(result, eventMessage)});
```

Подписываемся на вызов метода:
```
#!javascript
Guzzle.Counter.get.on('response', function(result) {console.log(result)});
```

Теперь каждый раз при вызове метода данный обработчик будет отрабатывать.
Если при вызове будет передан собственный обработчки как в примере выше будут запущены все.