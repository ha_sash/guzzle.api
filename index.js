"use strict";

if (global.Guzzle === undefined) {
    global.Guzzle = {};
}

if (Guzzle.APIManager === undefined) {
    /**
     * Менеджер API
     * @type {Guzzle.API}
     */
    Guzzle.APIManager = class APIManager {
        static get io() {
            return this._io;
        }

        static set io(io) {
            return this._io = io;
        }

        static get url() {
            return this._url || 'http://localhost:3000/';
        }

        static set url(url) {
            return this._url = url;
        }

        static get sessionCookieName() {
            return this._sessionCookieName || 'connect.sid';
        }

        static set sessionCookieName(name) {
            return this._sessionCookieName = name;
        }

        static get sessionSecret() {
            return this._sessionSecret || '';
        }

        static set sessionSecret(secret) {
            return this._sessionSecret =secret;
        }

        static get browserSocketVariableName() {
            return this._browserSocketVariableName || null;
        }

        static set browserSocketVariableName(name) {
            return this._browserSocketVariableName = name;
        }

        static get responseEventName() {
            return this._responseEventName || 'api:response';
        }

        static set responseEventName(name) {
            return this._responseEventName = name;
        }

        static get errorEventName() {
            return this._errorEventName || 'api:error';
        }

        static set errorEventName(name) {
            return this._errorEventName = name;
        }

        static get callEventName() {
            return this._callEventName || 'api:call';
        }

        static set callEventName(name) {
            return this._callEventName = name;
        }

        static get namespace() {
            return this._namespace || 'Guzzle';
        }

        static set namespace(ns) {
            return this._namespace = ns;
        }

        static init(io) {

            this.errors = {
                api_call_msg_id_not_found: {
                    type: 'global_error',
                        msg:  'Not found api call id.'
                },
                api_call_msg_action_not_found: {
                    type: 'global_error',
                        msg:  'Action {action} is not found.'
                },
                api_call_action_object_not_found: {
                    type: 'global_error',
                        msg:  'Not found action object by action name {action}.'
                },
                api_call_msg_method_not_found: {
                    type: 'global_error',
                        msg:  'Not found api call method.'
                },
                api_call_method_not_found_in_object: {
                    type: 'global_error',
                        msg:  'Not found action method by method name {method} in action {action}.'
                },
                api_call_msg_args_not_found: {
                    type: 'global_error',
                        msg:  'Not found api call args.'
                },
                api_call_msg_args_is_not_array: {
                    type: 'global_error',
                        msg:  'Not valid api call args for action {action} and method {method}.'
                }
            };

            this.resultObject = function(api, msg, socket) {
                var data, success = true, message = 'Empty result', extra = {};

                this.isResult = function() {
                    return true;
                }

                this.setData = function(_data) {
                    if (message === 'Empty result') {
                        message = 'Ok';
                    }
                    data = _data;
                    return this;
                }

                this.getData = function() {
                    return data;
                }

                this.setMessage = function(_message) {
                    message = _message;
                    return this;
                }

                this.getMessage = function() {
                    return message;
                }

                this.setSuccess = function(_success) {
                    success = _success;
                    return this;
                }

                this.isSuccess = function() {
                    return success;
                }

                this.addParam  = function(name, value) {
                    extra[name] = value;
                }

                this.getExtraParams = function () {
                    return extra;
                }

                this.send = function() {
                    api.sendResponse(this, msg, socket);
                }

                this.getSocket = function() {
                    return socket;
                }

                this.getCurrentUser = function() {
                    if (socket.session && socket.session.passport && socket.session.passport.user) {
                        if (socket.session.passport.user._id) {
                            socket.session.passport.user.logged_in = true;
                        }
                        return socket.session.passport.user;
                    } else {
                        return {logged_in:false};
                    }
                }

                this.getSession = function() {
                    return socket.session;
                }
            };

            this.actions = {};
            this.io = io;
            this.initListeners();
        }

        /**
         * Отправляем ответ
         * @param response {object}
         */
        static sendResponse(response, msg, socket) {
            var extra = response.getExtraParams(), result = {
                event:   this.responseEventName,
                id:      msg.id,
                result:  response.getData(),
                success: response.isSuccess(),
                msg:     response.getMessage()
            };

            if (extra) {
                for (let i in extra) {
                    if (extra.hasOwnProperty(i) && !result.hasOwnProperty(i)) {
                        result[i] = extra[i];
                    }
                }
            }

            socket.json.send(result);
        }

        /**
         * Отправка ошибки
         * @param err
         * @param msg
         */
        static sendError(err, msg, socket) {
            var msg = {};
            if (err instanceof Object) {
                msg = err;
            } else if (typeof err === 'string' && this.errors[err]) {
                var tmpl = this.errors[err], text = tmpl.msg;

                for (var i in msg) {
                    text.replace('{'+i+'}', msg[i]);
                }

                msg = {
                    type: tmpl.type,
                    msg: tmpl.msg,
                    code: err
                };
            } else {

            }

            socket.json.send(msg);
        }

        /**
         * Проверка входящего сообщения
         * @param msg {object}
         * @return {boolean}
         */
        static validateMessage(msg, socket) {
            var result = false, err = false;
            if (msg.id === undefined)                        err = 'api_call_msg_id_not_found';
            else if (msg.action === undefined)               err = 'api_call_msg_action_not_found';
            else if (this.actions[msg.action] === undefined) err = 'api_call_action_object_not_found';
            else if (msg.method === undefined)               err = 'api_call_msg_method_not_found';
            else if (msg.args === undefined)                 err = 'api_call_msg_args_not_found';
            else if (!Array.isArray(msg.args))               err = 'api_call_msg_args_is_not_array';
            else if (!this.actions[msg.action].apiMethods().hasOwnProperty(msg.method) || this.actions[msg.action][msg.method] === undefined || typeof this.actions[msg.action][msg.method] !== 'function')
                err = 'api_call_method_not_found_in_object';
            else
                result = true;

            if (err) {
                this.sendError(err, msg, socket);
            }

            return result;
        }

        /**
         * Обработка сообщения
         * @param msg - сообщение
         * @param socket - объект соединения
         */
        static onMessage(msg, socket) {
            if (msg.event == this.callEventName) {
                if (this.validateMessage(msg, socket)) {
                    var api = this.actions[msg.action], result = this.createResult(msg, socket);

                    try {
                        api[msg.method].apply(api, msg.args.concat(result));
                    } catch (e) {
                        result.setSuccess(false).setMessage(e.message).send();
                    }
                }
            }
        }


        static createResult(msg, socket) {
            return new this.resultObject(this, msg, socket);
        }

        /**
         * Запускаем прослушку сообщений с попытками вызовов.
         */
        static initListeners() {
            var me = this;
            this.io.sockets.on('connection', function(socket) {
                socket.on('message', function(msg) {
                    me.onMessage(msg, socket);
                });
            });
        }

        /**
         * Добавляем один или несколько объектов в API.
         * @param actionName {string}|{object}
         * @param object {Objcet}
         */
        static add(actionName, object) {
            if (object === undefined && actionName instanceof Object) {
                for(let i in actionName) {
                    this.add(i, actionName[i]);
                }
            } else if (this.actions[actionName] === undefined && object !== undefined && object instanceof Object) {
                if (object.apiMethods === undefined || typeof object.apiMethods !== 'function') {
                    throw new Error('API ('+actionName+') object has to have a method "apiMethods"');
                }
                this.actions[actionName] = object;
            }
        }

        /**
         * Получаем скрипт для генерации обертки API на стороне браузера
         * @return {string}
         */
        static getScript() {
            var script = [], result = {
                namespace         : this.namespace,
                url               : this.url,
                calleventname     : this.callEventName,
                responseeventname : this.responseEventName,
                erroreventname    : this.errorEventName,
                actions           : {}
            };

            for(var i in this.actions) {
                result.actions[i] = this.getMethods(this.actions[i]);
            }

            script.push(
                '(function() {',
                'var GAPIManager = new GuzzleAPI(',
                JSON.stringify(result, true, '    '),
                this.browserSocketVariableName === null ? '' : ', ' + this.browserSocketVariableName,
                ');',
                '})();'
            );

            return script.join('');
        }

        /**
         * Список методов и их аргументов
         * @param action
         * @return {Array}
         */
        static getMethods(action) {
            var methods = [], publicMethods = action.apiMethods();

            for (let i in publicMethods) {
                if (publicMethods[i] && action[i] instanceof Function) {
                    let args = this.getArtuments(action[i]);
                    methods.push({
                        method: i,
                        arguments: args.slice(0, args.length-1)
                    });
                }
            }

            return methods;
        }

        /**
         * Список аргументов функции
         * @param fn
         * @return {Array}
         */
        static getArtuments(fn) {
            var strFn = fn.toString(), fnHeader = strFn.match(/^[a-z0-9_]+(?:\s|)\((.*?)\)/gi);
            if (fnHeader && fnHeader[0]) {
                return fnHeader[0].replace(/^[a-z0-9_]+(?:\s|)\(/gi, '').replace(/\)/g, '').split(', ');
            }

            return [];
        }

    }
}

module.exports.getPublicPath = function() {
    return __dirname + '/public';
};